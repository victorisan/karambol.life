---
title: Karambol
author: victor
sections: tags
---


# About this Website

The backstory on how I came to own this domain is quite straightforward. One day I had something important to finish until the following day (don’t remember what that something was) and naturally I started procrastinating. By procrastinating I mean that I bought this specific domain and made some email aliases with it. In the end I decided to also build a website for shits and giggles, and here we are…
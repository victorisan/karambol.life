---
title: Hello World!
author: victor
date: 2023-05-28T18:48:00+03:00
---

# Welcome to karambol.life!

To be completely honest, I don't rightly know myself why this website exists. I figured that after playing around with [Hugo](https://gohugo.io) for a bit, I might as well create a blog site for whenever I feel like writing. Moreover, I will have my friends write whatever they want in here, just so it doesn't get too lonely... 

By the way, if you want to get your own page or even your own section on this website, feel free to submit a pull request on the [gitlab repo](https://gitlab.com/victorisan/karambol.life) and I'll be sure to check it out!

That would be all for now, see ya.
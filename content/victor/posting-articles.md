---
title: Posting Articles
author: victor
date: 2023-09-21
---

# Ever wanted to have a blog?

As I have said in my previous article, anyone is welcome to post their own stuff on this website. If you wish to write about anything at all, I highly encourage you to contact me via email: [victor@isan.ro](mailto:victor@isan.ro). Here's my [GPG](https://victor.isan.ro/victorisan.gpg) key.

## Guidelines for writing

1. Please use [markdown](https://www.markdownguide.org/cheat-sheet) when writing your article. If you've never heard of it, no problem! Markdown is pretty much the same as plain text, only with some added syntax that allows you to add headings, bold/italics, links, images and more...

2. Please include your name and any links (social media, website, donations, etc.) you want to have on your page in your email.

3. Please attach any images you want to use in your email along with the markdown file of your article.

Thank you to everyone who contributes!
---
title: (Im)Mortal
author: luca
date: 2024-01-17
caption: Medieval fantasy story by Luca
---

# Foreword
Jack was just going home from his weekly D&D game, whilst thinking about what his next character was gonna be. The last thing he expected was to get pulled into that character’s shoes. His very own Isekai, where he encounters the usual stuff: life-threatening situations, red-haired beauties and powers that are beyond anything a mortal could conceive. The only thing standing between Jack and his dream of living his life in a medieval fantasy world, is reality (or rather the make-believe reality he now finds himself in). 
